package com.svs.l17_t1.ui.list.adapter;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.svs.l17_t1.R;

class RealtyHolder extends RecyclerView.ViewHolder {
    ImageView mPhotoRealty;
    TextView mTitleTextView;
    TextView mDescriptionTextView;
    Button mDeleteButton;

    public RealtyHolder(View view) {
        super(view);
        mPhotoRealty = view.findViewById(R.id.iv_realty_photo);
        mTitleTextView = view.findViewById(R.id.tv_realty_title);
        mDescriptionTextView = view.findViewById(R.id.tv_realty_description);
        mDeleteButton = view.findViewById(R.id.btn_realty_delete);
    }
}