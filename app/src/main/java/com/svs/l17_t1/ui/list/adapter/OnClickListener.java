package com.svs.l17_t1.ui.list.adapter;

import com.svs.l17_t1.model.realty.Realty;

public interface OnClickListener {
    void onClick(int position);
}
