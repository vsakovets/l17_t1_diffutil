package com.svs.l17_t1.ui.list;


import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.svs.l17_t1.R;
import com.svs.l17_t1.model.realty.Realty;
import com.svs.l17_t1.model.realty.RealtyConstructor;
import com.svs.l17_t1.ui.list.adapter.OnClickListener;
import com.svs.l17_t1.ui.list.adapter.RealtyAdapter;
import com.svs.l17_t1.utils.AlertDialogUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class RealtyListFragment extends Fragment implements OnClickListener {

    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RealtyAdapter mAdapter;
    private FloatingActionButton mAdd;
    private List<Realty> mRealties;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_realty, container, false);
        mRecyclerView = view.findViewById(R.id.realty_recycler_view);
        mAdd = view.findViewById(R.id.button_add);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        setupRecyclerView();
        setupSwipeToRefresh();
        setupAddButton();
        return view;
    }

    @Override
    public void onClick(final int position) {
        AlertDialogUtils.showAlertDialog(
                getContext(),
                "Do yo want to delete realty?",
                "Yes",
                "No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteRealty(position);
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
    }

    private void setupRecyclerView() {
        mRealties = RealtyConstructor.generateNewItems();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new RealtyAdapter(mRealties);
        mAdapter.setDeleteBtnListener(this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setupSwipeToRefresh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mRealties = RealtyConstructor.generateNewItems();
                mAdapter.onNewData(mRealties);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void setupAddButton() {
        mAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addRealty();
            }
        });
    }

    private void addRealty() {
        ArrayList<Realty> newData = new ArrayList<>(mRealties);
        newData.add(RealtyConstructor.generateItem());
        mAdapter.onNewData(newData);//Auto scrool to last item
        mRealties.clear();
        mRealties.addAll(newData);
        mRecyclerView.smoothScrollToPosition(mRealties.size() - 1);
    }

    private void deleteRealty(int position) {
        ArrayList<Realty> newData = new ArrayList<>(mRealties);
        newData.remove(position);
        mAdapter.onNewData(newData);//Auto scrool to last item
        mRealties.clear();
        mRealties.addAll(newData);
        mRecyclerView.smoothScrollToPosition(mRealties.size() - 1);
    }
}
