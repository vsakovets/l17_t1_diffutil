package com.svs.l17_t1.ui.list.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.svs.l17_t1.R;
import com.svs.l17_t1.model.realty.Realty;

import java.util.List;

public class RealtyAdapter extends RecyclerView.Adapter<RealtyHolder> {

    private OnClickListener deleteBtnListener = null;

    public RealtyAdapter(List<Realty> mRealties) {
        this.mRealties = mRealties;
    }

    private List<Realty> mRealties;

    @NonNull
    @Override
    public RealtyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_realty, parent, false);
        return new RealtyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RealtyHolder holder, final int position) {
        holder.mPhotoRealty.setImageResource(mRealties.get(position).getImgId());
        holder.mTitleTextView.setText(mRealties.get(position).getTitle());
        holder.mDescriptionTextView.setText(mRealties.get(position).getDescription());
    }

    @Override
    public void onBindViewHolder(
            @NonNull final RealtyHolder holder,
            final int position,
            List<Object> payloads
    ) {
        holder.mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDeleteItem(v, holder.getAdapterPosition());
            }
        });
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {
            Bundle o = (Bundle) payloads.get(0);
            for (String key : o.keySet()) {
                if (key.equals("imageId")) {
                    holder.mPhotoRealty.setImageResource(mRealties.get(position).getImgId());
                }
                if (key.equals("title")) {
                    holder.mTitleTextView.setText(mRealties.get(position).getTitle());
                }
                if (key.equals("description")) {
                    holder.mDescriptionTextView.setText(mRealties.get(position).getDescription());
                }
            }
        }
    }

    private void onDeleteItem(View v, int position) {
        if (mRealties.size() > 0) {
            if (deleteBtnListener != null) {
                deleteBtnListener.onClick(position);
            }
        } else Toast.makeText(v.getContext(), "List is empty", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return mRealties.size();
    }

    public void setDeleteBtnListener(OnClickListener deleteBtnListener) {
        this.deleteBtnListener = deleteBtnListener;
    }
//
    public void onNewData(List<Realty> newData) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new MyDiffUtilCallback(newData, mRealties));
        mRealties.clear();
        mRealties.addAll(newData);
        diffResult.dispatchUpdatesTo(this);
    }


    public List<Realty> getData() {
        return mRealties;
    }
}
