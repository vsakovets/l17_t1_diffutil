package com.svs.l17_t1.ui.list.adapter;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.svs.l17_t1.model.realty.Realty;

import java.util.ArrayList;
import java.util.List;

public class MyDiffUtilCallback extends DiffUtil.Callback {
    List<Realty> newList;
    List<Realty> oldList;

    public MyDiffUtilCallback(List<Realty> newList, List<Realty> oldList) {
        this.newList = newList;
        this.oldList = oldList;
    }

    @Override
    public int getOldListSize() {
        return oldList != null ? oldList.size() : 0;
    }

    @Override
    public int getNewListSize() {
        return newList != null ? newList.size() : 0;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return newList.get(newItemPosition).getId()==oldList.get(oldItemPosition).getId() ;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return true;
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        Realty newRealty = newList.get(newItemPosition);
        Realty oldRealty = oldList.get(oldItemPosition);

        Bundle diff = new Bundle();
        if (!newRealty.getId().equals(oldRealty.getId())) {
            diff.putLong("id", newRealty.getId());
        }
        if (!(newRealty.getImgId() == oldRealty.getImgId())) {
            diff.putLong("imageId", newRealty.getImgId());
        }
        if (!newRealty.getTitle().equals(oldRealty.getTitle())) {
            diff.putString("title", newRealty.getTitle());
        }
        if (!newRealty.getDescription().equals(oldRealty.getDescription())) {
            diff.putString("description", newRealty.getDescription());
        }
        if (diff.size() == 0) {
            return null;
        }
        return diff;
    }
}