package com.svs.l17_t1.model.realty;

import com.svs.l17_t1.R;

import java.util.ArrayList;

public class RealtyConstructor {

    public static ArrayList<Realty> generateNewItems() {
        ArrayList<Realty> result = new ArrayList<>();
        for (int i = 0; i <6; i++) {
            result.add(generateItem());
        }
        return result;
    }

    public static Realty generateItem() {
        String[] realt = {"House", "Cottage", "Summer house", "chalet"};
        int[] draw = {R.drawable.ic_domain, R.drawable.ic_home, R.drawable.ic_house};
        Realty realty = new Realty();
        realty.setId(System.currentTimeMillis());
        realty.setImgId(draw[(int) (Math.random() * 2)]);
        realty.setTitle(realt[(int) (Math.random() * 3)]);
        realty.setDescription("floors: " + (int) ((Math.random() * 3) + 1) + "\nrooms: " + (int) ((Math.random() * 6) + 1));
        return realty;
    }
}
