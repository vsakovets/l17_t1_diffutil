package com.svs.l17_t1.model.realty;

public class Realty implements Comparable {
    private Long id;
    private int mImgId;
    private String mTitle;
    private String mDescription;


    public Realty(Long id, Integer imgId, String title, String description) {
        id = id;
        mImgId = imgId;
        mTitle = title;
        mDescription = description;
    }

    public Realty() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getImgId() {
        return mImgId;
    }

    public void setImgId(int imgId) {
        mImgId = imgId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    @Override
    public int compareTo(Object o) {
        Realty compare = (Realty) o;

        if (compare.getId().equals(this.id)
                && compare.getTitle().equals(this.getTitle())) {
            return 0;
        }
        return 1;
    }
}
