package com.svs.l17_t1.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class AlertDialogUtils {

    public static void showAlertDialog(
            final Context context,
            String message,
            String positiveButtonText,
            String negativeButtonText,
            DialogInterface.OnClickListener onPositiveClicked,
            DialogInterface.OnClickListener onNegativeClicked
    ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setPositiveButton(positiveButtonText, onPositiveClicked)
                .setNegativeButton(negativeButtonText, onNegativeClicked)
                .setCancelable(false)
                .show();
    }
}
